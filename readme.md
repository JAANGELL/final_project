This project analyzes Atlantic basin tropical cyclones.

It uses the HURDAT2 dataset from the NHC website at
https://www.nhc.noaa.gov/data/#hurdat. 
It is saved as "HURDAT2.txt" in Final_Project

This repo consists of an Environment.yml, 
a readme.md, and the HURDAT2 dataset (6.2 mb).


This project uses pandas to convert the dataset
into a dataframe that is easier to use and more nice looking.
Once the dataframe is made, it is easier to do analysis.
Matplotlib and cartopy were used to plot tropical 
cyclone storm tracks, along with hurricanes,
tropical storms, and tropical depression storm tracks. 

After that, the dataframe was used to make bar graphs
of different category storms, using a loop to go through
each wind speed and storm status. 
